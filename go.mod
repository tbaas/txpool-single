module chainmaker.org/chainmaker/txpool-single/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	chainmaker.org/chainmaker/utils/v2 v2.2.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/mitchellh/mapstructure v1.4.2
	github.com/prometheus/client_golang v1.11.0
	github.com/stretchr/testify v1.7.0
)
