/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"fmt"
	"math"
	"sync"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
)

type txValidateFunc func(tx *commonPb.Transaction, source protocol.TxSource) error

// txQueue contains config and common tx queue
type txQueue struct {
	log protocol.Logger

	commonTxQueue *txList   // common transaction queue
	configTxQueue *txList   // config transaction queue
	pendingCache  *sync.Map // Caches transactions that are already in the block to be deleted
}

// newQueue creates txQueue
func newQueue(blockStore protocol.BlockchainStore, log protocol.Logger) *txQueue {
	pendingCache := sync.Map{}
	queue := txQueue{
		log:           log,
		pendingCache:  &pendingCache,
		commonTxQueue: newTxList(log, &pendingCache, blockStore),
		configTxQueue: newTxList(log, &pendingCache, blockStore),
	}
	return &queue
}

// addTxsToConfigQueue add txs to config queue
func (queue *txQueue) addTxsToConfigQueue(memTxs *mempoolTxs) {
	queue.configTxQueue.Put(memTxs.mtxs, memTxs.source, nil)
}

// addTxsToCommonQueue add txs to common queue
func (queue *txQueue) addTxsToCommonQueue(memTxs *mempoolTxs) {
	queue.commonTxQueue.Put(memTxs.mtxs, memTxs.source, nil)
}

// deleteTxsInPending delete txs in pending
func (queue *txQueue) deleteTxsInPending(txIds []*commonPb.Transaction) {
	for _, tx := range txIds {
		queue.pendingCache.Delete(tx.Payload.TxId)
	}
}

// get gets tx from txQueue
func (queue *txQueue) get(txId string) (mtx *memTx, inBlockHeight uint64) {
	if mtx, inBlockHeight = queue.commonTxQueue.Get(txId); mtx != nil {
		return mtx, inBlockHeight
	}
	if mtx, inBlockHeight = queue.configTxQueue.Get(txId); mtx != nil {
		return mtx, inBlockHeight
	}
	return nil, math.MaxUint64
}

// getCommonTxs get common txs from txQueue
func (queue *txQueue) getCommonTxs(txIds []string) (map[string]*memTx, map[string]uint64) {
	return queue.commonTxQueue.GetTxs(txIds)
}

// configTxsCount return config tx count
func (queue *txQueue) configTxsCount() int {
	return queue.configTxQueue.Size()
}

// commonTxsCount return common tx count
func (queue *txQueue) commonTxsCount() int {
	return queue.commonTxQueue.Size()
}

// deleteConfigTxs delete config txs in txQueue
func (queue *txQueue) deleteConfigTxs(txIds []string) {
	queue.configTxQueue.Delete(txIds)
}

// deleteCommonTxs delete common txs in txQueue
func (queue *txQueue) deleteCommonTxs(txIds []string) {
	queue.commonTxQueue.Delete(txIds)
}

// fetch get txs from txQueue
func (queue *txQueue) fetch(expectedCount int, blockHeight uint64,
	validateTxTime func(tx *commonPb.Transaction) error) []*memTx {
	// 1. fetch the config transaction
	if configQueueLen := queue.configTxsCount(); configQueueLen > 0 {
		if mtxs, txIds := queue.configTxQueue.Fetch(1, validateTxTime, blockHeight); len(mtxs) > 0 {
			queue.log.Debugw("FetchTxBatch get config mtxs", "txCount", 1, "configQueueLen",
				configQueueLen, "txsLen", len(mtxs), "txIds", txIds)
			return mtxs
		}
	}
	// 2. fetch the common transaction
	if txQueueLen := queue.commonTxsCount(); txQueueLen > 0 {
		if mtxs, txIds := queue.commonTxQueue.Fetch(expectedCount, validateTxTime, blockHeight); len(mtxs) > 0 {
			queue.log.Debugw("FetchTxBatch get common mtxs", "txCount", expectedCount, "txQueueLen",
				txQueueLen, "txsLen", len(mtxs), "txIds", txIds)
			return mtxs
		}
	}
	return nil
}

// appendTxsToPendingCache append txs to pendingCache
func (queue *txQueue) appendTxsToPendingCache(txs []*commonPb.Transaction, blockHeight uint64, enableSqlDB bool) {
	if (utils.IsConfigTx(txs[0]) ||
		utils.IsManageContractAsConfigTx(txs[0], enableSqlDB) ||
		utils.IsManagementTx(txs[0])) &&
		len(txs) == 1 {
		queue.configTxQueue.appendTxsToPendingCache(txs, blockHeight)
	} else {
		queue.commonTxQueue.appendTxsToPendingCache(txs, blockHeight)
	}
}

// has whether has tx in txQueue
func (queue *txQueue) has(tx *commonPb.Transaction, checkPending bool) bool {
	if queue.commonTxQueue.Has(tx.Payload.TxId, checkPending) {
		return true
	}
	return queue.configTxQueue.Has(tx.Payload.TxId, checkPending)
}

func (queue *txQueue) status() string {
	return fmt.Sprintf("common txs len: %d, config txs len: %d", queue.commonTxQueue.Size(), queue.configTxQueue.Size())
}
