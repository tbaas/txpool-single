/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
)

type mempoolTxs struct {
	isConfigTxs bool
	mtxs        []*memTx
	source      protocol.TxSource
}

type memTx struct {
	tx            *commonPb.Transaction
	dbHeight      uint64 // db height when validate tx exist in full db
	inBlockHeight uint64 // block height when tx is packed into a block
}

func newMemTx(tx *commonPb.Transaction, dbHeight uint64) *memTx {
	return &memTx{
		tx:       tx,
		dbHeight: dbHeight,
	}
}

func (mtx *memTx) getChainId() string {
	return mtx.tx.Payload.ChainId
}

func (mtx *memTx) getTxId() string {
	return mtx.tx.Payload.TxId
}

func (mtx *memTx) getTx() *commonPb.Transaction {
	return mtx.tx
}
