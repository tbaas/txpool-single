/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"fmt"
	"sync"
	"testing"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

const contract = "userContract1"

func generateTxs(num int, isConfig bool) []*commonPb.Transaction {
	txs := make([]*commonPb.Transaction, 0, num)
	txType := commonPb.TxType_INVOKE_CONTRACT
	for i := 0; i < num; i++ {
		contractName := syscontract.SystemContract_CHAIN_CONFIG.String()

		if !isConfig {
			contractName = contract
		}
		txs = append(txs, &commonPb.Transaction{
			Payload: &commonPb.Payload{TxId: utils.GetRandTxId(), TxType: txType,
				Method: "SetConfig", ContractName: contractName},
		},
		)
	}
	return txs
}

func generateMemTxs(num int, isConfig bool) []*memTx {
	mtxs := make([]*memTx, 0, num)
	txType := commonPb.TxType_INVOKE_CONTRACT
	for i := 0; i < num; i++ {
		contractName := syscontract.SystemContract_CHAIN_CONFIG.String()

		if !isConfig {
			contractName = contract
		}
		mtx := &memTx{
			tx: &commonPb.Transaction{
				Payload: &commonPb.Payload{TxId: utils.GetRandTxId(), TxType: txType,
					Method: "SetConfig", ContractName: contractName},
			},
			dbHeight: 0,
		}
		mtxs = append(mtxs, mtx)
	}
	return mtxs
}

func getTxIdsByTxs(txs []*commonPb.Transaction) []string {
	txIds := make([]string, 0, len(txs))
	for _, tx := range txs {
		txIds = append(txIds, tx.Payload.TxId)
	}
	return txIds
}

func getTxIdsByMemTxs(mtxs []*memTx) []string {
	txIds := make([]string, 0, len(mtxs))
	for _, mtx := range mtxs {
		txIds = append(txIds, mtx.getTxId())
	}
	return txIds
}

var testListLogName = "test_tx_list"

func TestTxList_Put(t *testing.T) {
	// 0. init source
	mtxs := generateMemTxs(100, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, mockStore.store)

	// 1. put [RPC,P2P,INTERNAL] txs and check num in txList
	list.Put(mtxs[:10], protocol.RPC, nil)
	require.EqualValues(t, 10, list.Size())
	list.Put(mtxs[10:20], protocol.P2P, nil)
	require.EqualValues(t, 20, list.Size())
	list.Put(mtxs[20:30], protocol.INTERNAL, nil)
	require.EqualValues(t, 30, list.Size())

	// 2. repeat put tx failed due to the txs has exist in pool
	list.Put(mtxs[:10], protocol.RPC, nil)
	require.EqualValues(t, 30, list.Size())
	list.Put(mtxs[10:20], protocol.P2P, nil)
	require.EqualValues(t, 30, list.Size())
	list.Put(mtxs[20:30], protocol.INTERNAL, nil)
	require.EqualValues(t, 30, list.Size())

	// 5. put txs to pendingCache, so the txs from [RPC,P2P] can not put into pool,
	//but the txs from [INTERNAL] can put into pool
	for _, mtx := range mtxs[30:40] {
		list.pendingCache.Store(mtx.getTxId(), mtx)
	}
	list.Put(mtxs[30:40], protocol.RPC, nil)
	require.EqualValues(t, 30, list.Size())
	list.Put(mtxs[30:40], protocol.P2P, nil)
	require.EqualValues(t, 30, list.Size())
	list.Put(mtxs[30:40], protocol.INTERNAL, nil)
	require.EqualValues(t, 40, list.Size())
}

func TestTxList_Get(t *testing.T) {
	mtxs := generateMemTxs(100, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs[:30] into pool
	list.Put(mtxs[:30], protocol.RPC, nil)
	for _, mtx := range mtxs[:30] {
		tx, inBlockHeight := list.Get(mtx.getTxId())
		require.NotNil(t, tx)
		require.EqualValues(t, 0, inBlockHeight)
	}

	// 2. check mtxs[30:100] not exist in queue
	for _, mtx := range mtxs[30:100] {
		tx, inBlockHeight := list.Get(mtx.getTxId())
		require.Nil(t, tx)
		require.Greater(t, inBlockHeight, uint64(999))
	}

	// 3. put mtxs[30:40] to pending cache and check mtxs[30:40] exist in pendingCache
	for _, mtx := range mtxs[30:40] {
		mtx.inBlockHeight = 999
		list.pendingCache.Store(mtx.getTxId(), mtx)
	}
	for _, mtx := range mtxs[30:40] {
		txInPool, inBlockHeight := list.Get(mtx.getTxId())
		require.EqualValues(t, mtx, txInPool)
		require.EqualValues(t, 999, inBlockHeight)
	}
}

func TestTxList_GetTxs(t *testing.T) {
	mtxs := generateMemTxs(100, false)
	txIds := getTxIdsByMemTxs(mtxs)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs[:30] mtxs and check existence
	list.Put(mtxs[:30], protocol.RPC, nil)
	txsRet, inBlockHeightRet := list.GetTxs(txIds[:30])
	require.EqualValues(t, 30, len(txsRet))
	require.EqualValues(t, 30, len(inBlockHeightRet))

	// 2. check mtxs[30:100] not exist in txList
	txsRet, inBlockHeightRet = list.GetTxs(txIds[30:100])
	require.EqualValues(t, 0, len(txsRet))
	require.EqualValues(t, 70, len(inBlockHeightRet))
	require.Greater(t, inBlockHeightRet[txIds[30]], uint64(999))

	// 3. put mtxs[30:40] to pending cache and check mtxs[30:40] exist in pendingCache in the txList
	for _, mtx := range mtxs[30:40] {
		mtx.inBlockHeight = 999
		list.pendingCache.Store(mtx.getTxId(), mtx)
	}
	txsRet, inBlockHeightRet = list.GetTxs(txIds[30:40])
	require.EqualValues(t, 10, len(txsRet))
	require.EqualValues(t, 10, len(inBlockHeightRet))
	require.EqualValues(t, 999, inBlockHeightRet[txIds[30]])
}

func TestTxList_Has(t *testing.T) {
	mtxs := generateMemTxs(100, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs[:30] and check existence
	list.Put(mtxs[:30], protocol.RPC, nil)
	for _, mtx := range mtxs[:30] {
		require.True(t, list.Has(mtx.getTxId(), true))
		require.True(t, list.Has(mtx.getTxId(), false))
	}

	// 2. put mtxs[30:40] in pendingCache in txList and check existence
	for _, mtx := range mtxs[30:40] {
		mtx.inBlockHeight = 999
		list.pendingCache.Store(mtx.getTxId(), mtx)
	}
	for _, mtx := range mtxs[30:40] {
		require.True(t, list.Has(mtx.getTxId(), true))
		require.False(t, list.Has(mtx.getTxId(), false))
	}

	// 3. check not existence in txList
	for _, mtx := range mtxs[40:] {
		require.False(t, list.Has(mtx.getTxId(), true))
		require.False(t, list.Has(mtx.getTxId(), false))
	}
}

func TestTxList_Delete(t *testing.T) {
	mtxs := generateMemTxs(100, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs[:30]
	list.Put(mtxs[:30], protocol.RPC, nil)

	// 2. delete mtxs[:10] and check correctness
	list.Delete(getTxIdsByMemTxs(mtxs[:10]))
	require.EqualValues(t, 20, list.Size())
	for _, mtx := range mtxs[:10] {
		require.False(t, list.Has(mtx.getTxId(), true))
		require.False(t, list.Has(mtx.getTxId(), false))
	}

	// 2. put mtxs[30:50] in the pendingCache in txList
	for _, mtx := range mtxs[30:50] {
		mtx.inBlockHeight = 999
		list.pendingCache.Store(mtx.getTxId(), mtx)
	}

	// 3. put mtxs[40:50] succeed due to not check existence when source = [INTERNAL]
	list.Put(mtxs[40:50], protocol.INTERNAL, nil)
	require.EqualValues(t, 30, list.Size())

	// 4. delete mtxs[40:50], check pendingCache size and queue size
	list.Delete(getTxIdsByMemTxs(mtxs[40:50]))
	require.EqualValues(t, 20, list.Size())
}

func TestTxList_Fetch(t *testing.T) {
	mtxs := generateMemTxs(100, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs[:30] and Fetch mtxs
	list.Put(mtxs[:30], protocol.RPC, nil)

	fetchTxs, fetchTxIds := list.Fetch(100, nil, 99)
	require.EqualValues(t, 30, len(fetchTxs))
	require.EqualValues(t, 30, len(fetchTxIds))
	require.EqualValues(t, 0, list.Size())

	// 2. put mtxs[:30] failed due to exist in pendingCache
	list.Put(mtxs[:30], protocol.RPC, nil)
	require.EqualValues(t, 0, list.Size())

	// 3. fetch mtxs nil due to not exist mtxs in txList
	fetchTxs, fetchTxIds = list.Fetch(100, nil, 99)
	require.EqualValues(t, 0, len(fetchTxs))
	require.EqualValues(t, 0, len(fetchTxIds))

	// 4. put mtxs[30:100] and Fetch mtxs with less number
	list.Put(mtxs[30:], protocol.RPC, nil)
	fetchTxs, fetchTxIds = list.Fetch(10, nil, 111)
	require.EqualValues(t, 10, len(fetchTxs))
	require.EqualValues(t, 10, len(fetchTxIds))

	// 5. fetch all remaining mtxs
	fetchTxs, fetchTxIds = list.Fetch(100, nil, 112)
	require.EqualValues(t, 60, len(fetchTxs))
	require.EqualValues(t, 60, len(fetchTxIds))

	// 6. repeat put mtxs[30:100] with source = [INTERNAL] and fetch mtxs
	for _, mtx := range mtxs[30:] {
		list.pendingCache.Delete(mtx.getTxId())
	}
	list.Put(mtxs[30:], protocol.INTERNAL, nil)
	require.EqualValues(t, 70, list.Size())

	fetchTxs, fetchTxIds = list.Fetch(100, nil, 112)
	require.EqualValues(t, 70, len(fetchTxs))
	require.EqualValues(t, 70, len(fetchTxIds))
}

func TestTxList_Fetch_Bench(t *testing.T) {
	mtxs := generateMemTxs(1000000, false)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	blockChainStore := newMockBlockChainStore(ctrl, make(map[string]*commonPb.Transaction))
	list := newTxList(newMockLogger(ctrl, testListLogName), &sync.Map{}, blockChainStore.store)

	// 1. put mtxs
	beginPut := utils.CurrentTimeMillisSeconds()
	list.Put(mtxs, protocol.RPC, nil)
	fmt.Printf("put mtxs:%d, elapse time: %d\n", len(mtxs), utils.CurrentTimeMillisSeconds()-beginPut)

	// 2. fetch
	fetchNum := 100000
	for i := 0; i < len(mtxs)/fetchNum; i++ {
		beginFetch := utils.CurrentTimeMillisSeconds()
		fetchTxs, _ := list.Fetch(fetchNum, nil, 999)
		fmt.Printf("fetch mtxs:%d, elapse time: %d\n", len(fetchTxs), utils.CurrentTimeMillisSeconds()-beginFetch)
	}
	require.EqualValues(t, 0, list.queue.Size())
}
