/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"fmt"
	"math"
	"sync"

	"chainmaker.org/chainmaker/common/v2/linkedhashmap"
	"chainmaker.org/chainmaker/common/v2/monitor"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
	"github.com/prometheus/client_golang/prometheus"
)

// txList Structure of store transactions in memory
type txList struct {
	log              protocol.Logger
	blockchainStore  protocol.BlockchainStore
	metricTxPoolSize *prometheus.GaugeVec

	rwLock       sync.RWMutex
	queue        *linkedhashmap.LinkedHashMap // Orderly store *memTx. key: txId val:*memTx
	pendingCache *sync.Map                    // A place where transactions are stored after Fetch. key: txId val:*memTx
}

// newTxList creates txList
func newTxList(log protocol.Logger, pendingCache *sync.Map, blockchainStore protocol.BlockchainStore) *txList {
	list := &txList{
		log:             log,
		blockchainStore: blockchainStore,
		rwLock:          sync.RWMutex{},
		queue:           linkedhashmap.NewLinkedHashMap(),
		pendingCache:    pendingCache,
	}
	if MonitorEnabled {
		list.metricTxPoolSize = monitor.NewGaugeVec(
			monitor.SUBSYSTEM_TXPOOL,
			monitor.MetricTxPoolSize,
			monitor.HelpTxPoolSizeMetric,
			monitor.ChainId, monitor.PoolType)
	}
	return list
}

// Put Add transaction to the txList
func (l *txList) Put(mtxs []*memTx, source protocol.TxSource, validate txValidateFunc) {
	if len(mtxs) == 0 {
		return
	}
	for _, mtx := range mtxs {
		l.addTxs(mtx, source, validate)
	}
	l.monitor(mtxs[0], l.queue.Size())
}

// addTxs add txs to txList
func (l *txList) addTxs(mtx *memTx, source protocol.TxSource, validate txValidateFunc) {
	l.rwLock.Lock()
	defer l.rwLock.Unlock()
	if validate == nil || validate(mtx.getTx(), source) == nil {
		txId := mtx.getTxId()
		if source != protocol.INTERNAL {
			if val, ok := l.pendingCache.Load(txId); ok && val != nil {
				return
			}
		}
		if l.queue.Get(txId) != nil {
			return
		}
		l.queue.Add(txId, mtx)
	}
}

// Delete Delete transactions from TXList by the txIds
func (l *txList) Delete(txIds []string) {
	l.rwLock.Lock()
	defer l.rwLock.Unlock()
	for _, txId := range txIds {
		l.queue.Remove(txId)
		l.pendingCache.Delete(txId)
	}
}

// Fetch Gets a list of stored transactions
func (l *txList) Fetch(count int, validate func(tx *commonPb.Transaction) error, blockHeight uint64) (
	[]*memTx, []string) {
	queueLen := l.queue.Size()
	if queueLen < count {
		count = queueLen
	}
	var (
		mtxs    []*memTx
		txIds   []string
		errKeys []string
	)
	l.rwLock.Lock()
	defer func() {
		begin := utils.CurrentTimeMillisSeconds()
		for _, txId := range errKeys {
			l.queue.Remove(txId)
		}
		for _, mtx := range mtxs {
			txId := mtx.getTxId()
			l.queue.Remove(txId)
			l.pendingCache.Store(txId, mtx)
		}
		if len(mtxs) > 0 {
			l.monitor(mtxs[0], l.queue.Size())
		}
		l.rwLock.Unlock()
		l.log.Debugf("eliminate data, elapse time: %d", utils.CurrentTimeMillisSeconds()-begin)
	}()

	l.log.Debugw("txList Fetch", "count", count, "queueLen", queueLen)
	if count > 0 {
		mtxs, txIds, errKeys = l.getTxsFromQueue(count, blockHeight, validate)
		l.log.Debugw("txList Fetch txsNormal", "count", count, "queueLen", queueLen,
			"txsLen", len(mtxs), "errKeys", len(errKeys))
	}
	return mtxs, txIds
}

func (l *txList) getTxsFromQueue(count int, blockHeight uint64, validate func(tx *commonPb.Transaction) error) (
	mtxs []*memTx, txIds []string, errKeys []string) {
	mtxs = make([]*memTx, 0, count)
	txIds = make([]string, 0, count)
	errKeys = make([]string, 0, count)
	node := l.queue.GetLinkList().Front()
	for node != nil && count > 0 {
		txId, ok := node.Value.(string)
		if !ok {
			l.log.Errorf("interface value transfer into string failed")
		}
		mtx, ok1 := l.queue.Get(txId).(*memTx)
		if !ok1 {
			l.log.Errorf("interface val transfer into *commonPb.Transaction failed")
		}
		if validate != nil && validate(mtx.getTx()) != nil {
			errKeys = append(errKeys, txId)
		} else {
			// put block height to mtx
			mtx.inBlockHeight = blockHeight
			mtxs = append(mtxs, mtx)
			txIds = append(txIds, txId)
			if val, ok2 := l.pendingCache.Load(txId); ok2 {
				l.log.Errorf("tx:%s duplicate to package block, txInPoolHeight: %d",
					txId, val.(*memTx).inBlockHeight)
			}
			// count valid txs
			count--
		}
		node = node.Next()
	}
	return
}

func (l *txList) monitor(mtx *memTx, len int) {
	if MonitorEnabled {
		chainId := mtx.getChainId()
		if chainId != "" {
			if utils.IsConfigTx(mtx.getTx()) {
				go l.metricTxPoolSize.WithLabelValues(chainId, "config").Set(float64(len))
			} else {
				go l.metricTxPoolSize.WithLabelValues(chainId, "normal").Set(float64(len))
			}
		}
	}
}

// Has Determine if the transaction exists in the txList
func (l *txList) Has(txId string, checkPending bool) (exist bool) {
	if checkPending {
		if val, ok := l.pendingCache.Load(txId); ok && val != nil {
			return true
		}
	}
	l.rwLock.RLock()
	defer l.rwLock.RUnlock()
	return l.queue.Get(txId) != nil
}

// Get Retrieve the transaction from txList by the txId
// inBlockHeight: return math.MaxUint64 when the transaction does not exist,
// return 0 when the transaction is in the queue to wait to be generate block,
// return positive integer, indicating that the tx is in an unchained block.
func (l *txList) Get(txId string) (tx *memTx, inBlockHeight uint64) {
	if val, ok := l.pendingCache.Load(txId); ok && val != nil {
		if mtx, ok1 := val.(*memTx); ok1 {
			l.log.Debugw(fmt.Sprintf("txList Get tx by txId = %s in pendingCache", txId), "exist", true)
			return mtx, mtx.inBlockHeight
		}
		l.log.Errorf("interface val transfer into *memTx failed")
		l.pendingCache.Delete(txId)
	}

	l.rwLock.RLock()
	defer l.rwLock.RUnlock()
	if val := l.queue.Get(txId); val != nil {
		if mtx, ok := val.(*memTx); ok {
			l.log.Debugw(fmt.Sprintf("txList Get tx by txId = %s in queue", txId), "exist", true)
			return mtx, 0
		}
		l.log.Errorf("interface val transfer into *memTx failed")
		l.queue.Remove(txId)
	}
	l.log.Debugw(fmt.Sprintf("txList Get Transaction by txId = %s", txId), "exist", false)
	return nil, math.MaxUint64
}

// GetTxs Retrieve transactions from txList by the txIds
func (l *txList) GetTxs(txIds []string) (mtxsRet map[string]*memTx, txsHeightRet map[string]uint64) {
	mtxsRet = make(map[string]*memTx, len(txIds))
	txsHeightRet = make(map[string]uint64, len(txIds))
	txIdsNoPending := make([]string, 0, len(txIds))

	for _, txId := range txIds {
		if val, ok := l.pendingCache.Load(txId); ok && val != nil {
			if mtx, ok1 := val.(*memTx); ok1 {
				mtxsRet[txId] = mtx
				txsHeightRet[txId] = mtx.inBlockHeight
			} else {
				l.log.Errorf("interface pendingVal transfer into *memTx failed")
				l.pendingCache.Delete(txId)
			}
		} else {
			txIdsNoPending = append(txIdsNoPending, txId)
		}
	}

	l.rwLock.RLock()
	defer l.rwLock.RUnlock()
	for _, txId := range txIdsNoPending {
		if val := l.queue.Get(txId); val != nil {
			if mtx, ok := val.(*memTx); ok {
				mtxsRet[txId] = mtx
				txsHeightRet[txId] = 0
			} else {
				l.log.Errorf("interface val transfer into *memTx failed")
				l.queue.Remove(txId)
			}
		} else {
			txsHeightRet[txId] = math.MaxUint64
		}
	}
	l.log.Debugw("txList Get Transactions by txIds", "want:%d", len(txIds), "got:%d", len(mtxsRet))
	return mtxsRet, txsHeightRet
}

// appendTxsToPendingCache append txs to pendingCache
func (l *txList) appendTxsToPendingCache(txs []*commonPb.Transaction, blockHeight uint64) {
	l.rwLock.Lock()
	defer l.rwLock.Unlock()
	for _, tx := range txs {
		l.queue.Remove(tx.Payload.TxId)
		l.pendingCache.Store(tx.Payload.TxId, &memTx{tx: tx, inBlockHeight: blockHeight})
	}
}

// Size Gets the number of transactions stored in the txList
func (l *txList) Size() int {
	l.rwLock.RLock()
	defer l.rwLock.RUnlock()
	return l.queue.Size()
}
