/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"time"

	"chainmaker.org/chainmaker/protocol/v2"
)

const (
	DefaultChannelSize    = 10000 // The channel size to add the txs
	DefaultCacheThreshold = 1
	DefaultFlushTimeOut   = 2 * time.Second
	DefaultFlushTicker    = 2
)

const (
	DefaultMaxTxCount          = 1000         // Maximum number of transactions in a block
	DefaultMaxTxPoolSize       = 5120         // Maximum number of common transaction in the pool
	DefaultMaxConfigTxPoolSize = 100          // Maximum number of config transaction in the pool
	DefaultMaxTxTimeTimeout    = float64(600) // The unit is in seconds
)

var (
	TxPoolConfig   *txPoolConfig
	MonitorEnabled bool
)

type txPoolConfig struct {
	PoolType            string `mapstructure:"pool_type"`
	MaxTxPoolSize       uint32 `mapstructure:"max_txpool_size"`
	MaxConfigTxPoolSize uint32 `mapstructure:"max_config_txpool_size"`
	IsMetrics           bool   `mapstructure:"is_metrics"`
	Performance         bool   `mapstructure:"performance"`
	BatchMaxSize        int    `mapstructure:"batch_max_size"`
	BatchCreateTimeout  int64  `mapstructure:"batch_create_timeout"`
	CacheFlushTicker    int64  `mapstructure:"cache_flush_ticker"`
	CacheThresholdCount int64  `mapstructure:"cache_threshold_count"`
	CacheFlushTimeOut   int64  `mapstructure:"cache_flush_timeout"`
	AddTxChannelSize    int64  `mapstructure:"add_tx_channel_size"`
}

// ===========config in the blockchain============

// IsTxTimeVerify Whether transactions require validation
func IsTxTimeVerify(chainConf protocol.ChainConf) bool {
	if chainConf != nil {
		config := chainConf.ChainConfig()
		if config != nil {
			return config.Block.TxTimestampVerify
		}
	}
	return false
}

// MaxTxTimeTimeout The maximum timeout for a transaction
func MaxTxTimeTimeout(chainConf protocol.ChainConf) float64 {
	if chainConf != nil {
		config := chainConf.ChainConfig()
		if config != nil && config.Block.TxTimeout > 0 {
			return float64(config.Block.TxTimeout)
		}
	}
	return DefaultMaxTxTimeTimeout
}

// MaxTxCount Maximum number of transactions in a block
func MaxTxCount(chainConf protocol.ChainConf) int {
	if chainConf != nil {
		config := chainConf.ChainConfig()
		if config != nil && config.Block.BlockTxCapacity > 0 {
			return int(config.Block.BlockTxCapacity)
		}
	}
	return DefaultMaxTxCount
}

// ===========config in the local============

// MaxCommonTxPoolSize Maximum number of common transaction in the pool
func MaxCommonTxPoolSize() int {
	config := TxPoolConfig
	if config.MaxTxPoolSize != 0 {
		return int(config.MaxTxPoolSize)
	}
	return DefaultMaxTxPoolSize
}

// MaxConfigTxPoolSize The maximum number of configure transaction in the pool
func MaxConfigTxPoolSize() int {
	config := TxPoolConfig
	if config.MaxConfigTxPoolSize != 0 {
		return int(config.MaxConfigTxPoolSize)
	}
	return DefaultMaxConfigTxPoolSize
}

// IsMetrics Whether to log operation time
func IsMetrics() bool {
	config := TxPoolConfig
	return config.IsMetrics
}
