/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"errors"
	"fmt"
	"math"
	"sync"
	"sync/atomic"
	"time"

	commonErrors "chainmaker.org/chainmaker/common/v2/errors"
	"chainmaker.org/chainmaker/common/v2/monitor"
	"chainmaker.org/chainmaker/common/v2/msgbus"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	netPb "chainmaker.org/chainmaker/pb-go/v2/net"
	txpoolPb "chainmaker.org/chainmaker/pb-go/v2/txpool"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
	"github.com/gogo/protobuf/proto"
	"github.com/mitchellh/mapstructure"
	"github.com/prometheus/client_golang/prometheus"
)

const TxPoolType = "SINGLE"

var _ protocol.TxPool = (*txPoolImpl)(nil)

// txPoolImpl is single txPool implement
type txPoolImpl struct {
	chainId      string
	queue        *txQueue            // the queue for store transactions
	cache        *txCache            // the cache to temporarily cache transactions
	addTxsCh     chan *mempoolTxs    // channel that receive the common transactions
	stopCh       chan struct{}       // the channel signal that stop the service
	stopAtomic   int64               // the flag that identifies whether the service has closed
	flushTicker  int                 // ticker to check whether the cache needs to be refreshed
	signalLock   sync.RWMutex        // Locker to protect signal status
	signalStatus txpoolPb.SignalType // The current state of the transaction pool

	ac              protocol.AccessControlProvider
	log             protocol.Logger
	msgBus          msgbus.MessageBus  // Information interaction between modules
	chainConf       protocol.ChainConf // chainConfig
	txFilter        protocol.TxFilter
	blockchainStore protocol.BlockchainStore // Store module implementation

	metricTxExistInDBTime  *prometheus.HistogramVec
	metricTxVerifySignTime *prometheus.HistogramVec
}

// NewTxPoolImpl creates txPoolImpl
func NewTxPoolImpl(
	nodeId string,
	chainId string,
	txFilter protocol.TxFilter,
	blockStore protocol.BlockchainStore,
	msgBus msgbus.MessageBus,
	conf protocol.ChainConf,
	ac protocol.AccessControlProvider,
	log protocol.Logger,
	monitorEnabled bool,
	poolConfig map[string]interface{}) (protocol.TxPool, error) {
	if len(chainId) == 0 {
		return nil, fmt.Errorf("no chainId in create txpool")
	}

	MonitorEnabled = monitorEnabled
	TxPoolConfig = &txPoolConfig{}
	if err := mapstructure.Decode(poolConfig, TxPoolConfig); err != nil {
		return nil, err
	}
	var (
		ticker    = DefaultFlushTicker
		addChSize = DefaultChannelSize
	)
	if TxPoolConfig.AddTxChannelSize > 0 {
		addChSize = int(TxPoolConfig.AddTxChannelSize)
	}
	if TxPoolConfig.CacheFlushTicker > 0 {
		ticker = int(TxPoolConfig.CacheFlushTicker)
	}
	txPoolQueue := &txPoolImpl{
		chainId:      chainId,
		cache:        newTxCache(),
		stopCh:       make(chan struct{}),
		addTxsCh:     make(chan *mempoolTxs, addChSize),
		flushTicker:  ticker,
		signalStatus: txpoolPb.SignalType_NO_EVENT,

		ac:              ac,
		log:             log,
		msgBus:          msgBus,
		chainConf:       conf,
		txFilter:        txFilter,
		blockchainStore: blockStore,
	}
	txPoolQueue.queue = newQueue(blockStore, log)
	if MonitorEnabled {
		txPoolQueue.metricTxExistInDBTime = monitor.NewHistogramVec(
			monitor.SUBSYSTEM_TXPOOL,
			monitor.MetricValidateTxInDBTime,
			monitor.MetricValidateTxInDBTimeMetric,
			[]float64{0, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100},
			monitor.ChainId,
		)
		txPoolQueue.metricTxVerifySignTime = monitor.NewHistogramVec(
			monitor.SUBSYSTEM_TXPOOL,
			monitor.MetricValidateTxSignTime,
			monitor.MetricValidateTxSignTimeMetric,
			[]float64{0, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100},
			monitor.ChainId,
		)
	}
	return txPoolQueue, nil
}

// Start starts up txPool service
func (pool *txPoolImpl) Start() (err error) {
	if pool.msgBus != nil {
		pool.msgBus.Register(msgbus.RecvTxPoolMsg, pool)
	}
	go pool.listen()
	return
}

// listen consumes  put txs in addTxsCh to txCache
func (pool *txPoolImpl) listen() {
	flushTicker := time.NewTicker(time.Duration(pool.flushTicker) * time.Second)
	defer flushTicker.Stop()
	for {
		select {
		case memTxs := <-pool.addTxsCh:
			pool.flushOrAddTxsToCache(memTxs)
		case <-flushTicker.C:
			if pool.cache.isFlushByTime() && pool.cache.txCount() > 0 {
				pool.flushCommonTxToQueue(nil)
			}
		case <-pool.stopCh:
			return
		}
	}
}

// flushOrAddTxsToCache flush txs to txCache,
// and add config or common txs to txQueue
func (pool *txPoolImpl) flushOrAddTxsToCache(memTxs *mempoolTxs) {
	if memTxs == nil || len(memTxs.mtxs) == 0 {
		return
	}
	defer func() {
		pool.log.Debugf("txPool status: %s, cache txs num: %d", pool.queue.status(), pool.cache.txCount())
	}()
	// config tx
	if memTxs.isConfigTxs {
		pool.flushConfigTxToQueue(memTxs)
		return
	}
	// common txs
	if pool.cache.isFlushByTxCount(memTxs) {
		pool.flushCommonTxToQueue(memTxs)
	} else {
		pool.cache.addMemoryTxs(memTxs)
	}
}

// flushConfigTxToQueue add config tx to txQueue
func (pool *txPoolImpl) flushConfigTxToQueue(memTxs *mempoolTxs) {
	defer func() {
		pool.updateAndPublishSignal()
	}()
	pool.queue.addTxsToConfigQueue(memTxs)
}

// flushCommonTxToQueue add common txs to txQueue
func (pool *txPoolImpl) flushCommonTxToQueue(memTxs *mempoolTxs) {
	defer func() {
		pool.updateAndPublishSignal()
		pool.cache.reset()
	}()
	// split txs by source
	rpcTxs, p2pTxs, internalTxs := pool.cache.mergeAndSplitTxsBySource(memTxs)
	// add txs to txQueue
	pool.queue.addTxsToCommonQueue(&mempoolTxs{mtxs: rpcTxs, source: protocol.RPC})
	pool.queue.addTxsToCommonQueue(&mempoolTxs{mtxs: p2pTxs, source: protocol.P2P})
	pool.queue.addTxsToCommonQueue(&mempoolTxs{mtxs: internalTxs, source: protocol.INTERNAL})
}

// Stop stops txPool service
func (pool *txPoolImpl) Stop() error {
	if !atomic.CompareAndSwapInt64(&pool.stopAtomic, 0, 1) {
		return fmt.Errorf("txpool service has stoped")
	}
	close(pool.stopCh)
	close(pool.addTxsCh)
	pool.log.Infof("close txpool service")
	return nil
}

// AddTx add tx form RPC or P2P to pool
func (pool *txPoolImpl) AddTx(tx *commonPb.Transaction, source protocol.TxSource) error {
	// tx should not be nil
	if tx == nil {
		return commonErrors.ErrStructEmpty
	}
	// txPool should not be stopped
	if atomic.LoadInt64(&pool.stopAtomic) > 0 {
		pool.log.Info("AddTx TxPool has stopped")
		return errors.New("AddTx TxPool has stopped")
	}
	// should not be INTERNAL
	if source == protocol.INTERNAL {
		return commonErrors.ErrTxSource
	}

	// 0. Determine if the tx pool is full
	if pool.isFull(tx) {
		return commonErrors.ErrTxPoolLimit
	}
	// 1. Determine if the tx exist in pool
	if pool.TxExists(tx) {
		pool.log.Warnf("transaction exists in pool, txId: %s", tx.Payload.TxId)
		return commonErrors.ErrTxIdExist
	}
	// 2. Determine if the tx is out of date, signature/format is right(P2P), tx exist in db
	mtx, err := pool.validateTx(tx, source)
	if err != nil {
		return err
	}
	pool.log.Debugw("AddTx", "txId", tx.Payload.TxId, "source", source)

	// 3. marshal tx form RPC
	var txMsg []byte
	if source == protocol.RPC {
		if txMsg, err = proto.Marshal(tx); err != nil {
			pool.log.Errorf("broadcastTx proto.Marshal(tx) err: %v", err)
			return err
		}
	}
	// 4. store the transaction
	memTxs := &mempoolTxs{isConfigTxs: false, mtxs: []*memTx{mtx}, source: source}
	if utils.IsConfigTx(tx) ||
		utils.IsManageContractAsConfigTx(tx, pool.chainConf.ChainConfig().Contract.EnableSqlSupport) ||
		utils.IsManagementTx(tx) {
		memTxs.isConfigTxs = true
	}
	t := time.NewTimer(time.Second)
	defer t.Stop()
	select {
	case pool.addTxsCh <- memTxs:
	case <-t.C:
		pool.log.Warnf("add transaction timeout, txId:%s", tx.Payload.TxId)
		return fmt.Errorf("add transaction timeout, txId:%s", tx.Payload.TxId)
	}
	// 5. broadcast the transaction
	if source == protocol.RPC {
		pool.broadcastTx(tx.Payload.TxId, txMsg)
	}
	return nil
}

// isFull Check whether the transaction pool is full or not
func (pool *txPoolImpl) isFull(tx *commonPb.Transaction) bool {
	// config tx
	if utils.IsConfigTx(tx) ||
		utils.IsManageContractAsConfigTx(tx, pool.chainConf.ChainConfig().Contract.EnableSqlSupport) ||
		utils.IsManagementTx(tx) {
		if pool.queue.configTxsCount() >= MaxConfigTxPoolSize() {
			pool.log.Warnf("AddTx configTxPool is full, txId: %s, configQueueSize: %d", tx.Payload.GetTxId(),
				pool.queue.configTxsCount())
			return true
		}
		return false
	}
	// common tx
	if pool.queue.commonTxsCount() >= MaxCommonTxPoolSize() {
		pool.log.Warnf("AddTx txPool is full, txId: %s, txQueueSize: %d", tx.Payload.GetTxId(),
			pool.queue.commonTxsCount())
		return true
	}
	return false
}

// publish publishes txPool signal to core
func (pool *txPoolImpl) publish(signalType txpoolPb.SignalType) {
	if pool.msgBus != nil {
		pool.msgBus.Publish(msgbus.TxPoolSignal, &txpoolPb.TxPoolSignal{
			SignalType: signalType,
			ChainId:    pool.chainId,
		})
	}
}

// broadcastTx broadcast tx to other nodes
func (pool *txPoolImpl) broadcastTx(txId string, txMsg []byte) {
	if pool.msgBus != nil {
		pool.log.Debugf("broadcastTx txId: %s", txId)
		netMsg := &netPb.NetMsg{
			Payload: txMsg,
			Type:    netPb.NetMsg_TX,
		}
		pool.msgBus.Publish(msgbus.SendTxPoolMsg, netMsg)
	}
}

// updateAndPublishSignal When the number of transactions in the transaction pool is greater
// than or equal to the block can contain, update the status of the tx pool to block
// propose, otherwise update the status of tx pool to TRANSACTION_INCOME.
func (pool *txPoolImpl) updateAndPublishSignal() {
	signalType := txpoolPb.SignalType_NO_EVENT
	defer func() {
		if signalType != txpoolPb.SignalType_NO_EVENT {
			pool.log.Debugf("updateAndPublishSignal pool.publish signalType: %s", signalType)
			pool.publish(signalType)
		}
		pool.setSignalStatus(signalType)
	}()

	if pool.queue.configTxsCount() > 0 || pool.queue.commonTxsCount() >= MaxTxCount(pool.chainConf) {
		signalType = txpoolPb.SignalType_BLOCK_PROPOSE
	} else {
		signalType = txpoolPb.SignalType_TRANSACTION_INCOME
	}
}

// FetchTxBatch Get the batch of transactions from the tx pool to generate new block
func (pool *txPoolImpl) FetchTxBatch(blockHeight uint64) []*commonPb.Transaction {
	// fetch memTxs
	startFetchTime := utils.CurrentTimeMillisSeconds()
	mtxs := pool.queue.fetch(MaxTxCount(pool.chainConf), blockHeight, pool.validateTxTime)
	if len(mtxs) == 0 {
		return nil
	}

	// prune memTxs and return txs
	startPruneTime := utils.CurrentTimeMillisSeconds()
	txs := pool.pruneFetchedTxsExistDB(mtxs)

	endTime := utils.CurrentTimeMillisSeconds()
	pool.log.Infof("FetchTxBatch, height:%d, txs:[fetch:%d,indb:%d], elapse time:[fetch:%d,db:%d,total:%dms]",
		blockHeight, len(txs), len(mtxs)-len(txs),
		startPruneTime-startFetchTime, endTime-startPruneTime, endTime-startFetchTime)
	return txs
}

// pruneFetchedTxsExistDB prune fetched txs which exist in db
func (pool *txPoolImpl) pruneFetchedTxsExistDB(mtxs []*memTx) (txsRet []*commonPb.Transaction) {
	if len(mtxs) == 0 {
		return nil
	}
	// verify whether mtxs exist in the increment db
	workers := utils.CalcTxVerifyWorkers(len(mtxs))
	perWorkerTxsNum := len(mtxs) / workers

	txsRetTable := make([][]*commonPb.Transaction, workers)
	txsInDBTable := make([][]*commonPb.Transaction, workers)

	var wg sync.WaitGroup
	for i := 0; i < workers; i++ {
		wg.Add(1)
		txsPerWorker := mtxs[i*perWorkerTxsNum : (i+1)*perWorkerTxsNum]
		if i == workers-1 {
			txsPerWorker = mtxs[i*perWorkerTxsNum:]
		}
		go func(idx int, perMtxs []*memTx) {
			defer wg.Done()
			txs := make([]*commonPb.Transaction, 0, len(perMtxs))
			txsInDB := make([]*commonPb.Transaction, 0)
			for _, mtx := range perMtxs {
				tx := mtx.getTx()
				if !pool.isTxExistsInIncrementDB(tx, mtx.dbHeight) {
					txs = append(txs, tx)
				} else {
					txsInDB = append(txsInDB, tx)
				}
			}
			txsRetTable[idx] = txs
			txsInDBTable[idx] = txsInDB
		}(i, txsPerWorker)
	}
	wg.Wait()

	// delete tx in db and return txs not in db
	txsRet = make([]*commonPb.Transaction, 0, len(mtxs))
	for i := 0; i < workers; i++ {
		txsRet = append(txsRet, txsRetTable[i]...)
		// delete mtxs in the pool that have been commit
		// because the current exist tx whether in db is not in the queue.lock,
		// the proposer can not pack mtxs that have been commit
		pool.removeTxs(txsInDBTable[i])
	}
	return txsRet
}

// GetTxByTxId Retrieve the transaction by the txId from the txPool
func (pool *txPoolImpl) GetTxByTxId(txId string) (tx *commonPb.Transaction, inBlockHeight uint64) {
	mtx, height := pool.queue.get(txId)
	if mtx != nil {
		tx = mtx.getTx()
	}
	inBlockHeight = height
	return
}

// GetTxsByTxIds Retrieves the tx by the txIds from the tx pool.
// txsRet if the transaction is in the tx pool, it will be returned in txsRet.
// txsHeightRet if the transaction is in the pending queue of the tx pool,
// the corresponding block height when the transaction entered the block is returned,
// if the transaction is in the normal queue of the tx pool, the tx height is 0,
// if the transaction is not in the transaction pool, the tx height is math.MaxUint64.
func (pool *txPoolImpl) GetTxsByTxIds(txIds []string) (
	txsRet map[string]*commonPb.Transaction, txsHeightRet map[string]uint64) {
	if len(txIds) == 0 {
		return nil, nil
	}
	startGetTime := utils.CurrentTimeMillisSeconds()
	// may be config tx
	if len(txIds) == 1 {
		txsRet = make(map[string]*commonPb.Transaction, 1)
		txsHeightRet = make(map[string]uint64, 1)
		txId := txIds[0]
		var startPruneTime int64
		var inDBNum int32
		if mtx, inBlockHeight := pool.queue.get(txId); mtx != nil {
			startPruneTime = utils.CurrentTimeMillisSeconds()
			if !pool.isTxExistsInIncrementDB(mtx.getTx(), mtx.dbHeight) {
				txsRet[txId] = mtx.getTx()
				txsHeightRet[txId] = inBlockHeight
			} else {
				inDBNum++
				pool.removeTxs([]*commonPb.Transaction{mtx.getTx()})
				txsHeightRet[txId] = math.MaxUint64
			}
		}
		endTime := utils.CurrentTimeMillisSeconds()
		pool.log.Infof("GetTxsByTxIds, txs:[want:%d,get:%d indb:%d], elapse time: [get:%d,db:%d,total:%dms]",
			len(txIds), len(txsRet), inDBNum,
			startPruneTime-startGetTime, endTime-startPruneTime, endTime-startGetTime)
		return txsRet, txsHeightRet
	}

	// get common mtxs
	mtxsRet, mtxsHeightRet := pool.queue.getCommonTxs(txIds)

	// prune mtxs and return txsRet and txsHeightRet
	startPruneTime := utils.CurrentTimeMillisSeconds()
	txsRet, txsHeightRet = pool.pruneGetTxsExistDB(mtxsRet, mtxsHeightRet)

	endTime := utils.CurrentTimeMillisSeconds()
	pool.log.Infof("GetTxsByTxIds, txs:[want:%d,get:%d indb:%d], elapse time: [get:%d,db:%d,total:%dms]",
		len(txIds), len(txsRet), len(mtxsRet)-len(txsRet),
		startPruneTime-startGetTime, endTime-startPruneTime, endTime-startGetTime)
	return txsRet, txsHeightRet
}

// pruneGetTxsExistDB prune got txs which exist in db
func (pool *txPoolImpl) pruneGetTxsExistDB(mtxsMap map[string]*memTx, txsHeightRet map[string]uint64) (
	map[string]*commonPb.Transaction, map[string]uint64) {
	if len(mtxsMap) == 0 {
		return nil, txsHeightRet
	}

	// verify whether mtxs exist in db
	mtxs := convertMapToSlice(mtxsMap)
	workers := utils.CalcTxVerifyWorkers(len(mtxs))
	perWorkerMtxsNum := len(mtxs) / workers

	txsTable := make([][]*commonPb.Transaction, workers)
	txsInDBTable := make([][]*commonPb.Transaction, workers)

	var wg sync.WaitGroup
	for i := 0; i < workers; i++ {
		wg.Add(1)
		mtxsPerWorker := mtxs[i*perWorkerMtxsNum : (i+1)*perWorkerMtxsNum]
		if i == workers-1 {
			mtxsPerWorker = mtxs[i*perWorkerMtxsNum:]
		}
		go func(idx int, perMtxs []*memTx) {
			defer wg.Done()
			txs := make([]*commonPb.Transaction, 0, len(perMtxs))
			txsInDB := make([]*commonPb.Transaction, 0)
			for _, mtx := range perMtxs {
				tx := mtx.getTx()
				if pool.isTxExistsInIncrementDB(tx, mtx.dbHeight) {
					txsInDB = append(txsInDB, tx)
				} else {
					txs = append(txs, tx)
				}
			}
			txsTable[idx] = txs
			txsInDBTable[idx] = txsInDB
		}(i, mtxsPerWorker)
	}
	wg.Wait()

	txsRet := make(map[string]*commonPb.Transaction, len(mtxsMap))
	// delete tx in db and return txs not in db
	for i := 0; i < workers; i++ {
		for _, tx := range txsTable[i] {
			txsRet[tx.Payload.TxId] = tx
		}
		// delete txs in the pool that have been commit
		// because the current validate whether tx exist db is not in the queue.lock,
		// the proposer may packs txs that have been commit
		txsInDB := txsInDBTable[i]
		for _, tx := range txsInDB {
			txId := tx.Payload.TxId
			txsHeightRet[txId] = math.MaxUint64
		}
		pool.removeTxs(txsInDB)
	}
	return txsRet, txsHeightRet
}

// AddTxsToPendingCache These transactions will be added to the cache to avoid the transactions
// are fetched again and re-filled into the new block. Because Because of the chain confirmation
// rule in the HotStuff consensus algorithm.
func (pool *txPoolImpl) AddTxsToPendingCache(txs []*commonPb.Transaction, blockHeight uint64) {
	if len(txs) == 0 {
		return
	}
	startTime := utils.CurrentTimeMillisSeconds()
	pool.queue.appendTxsToPendingCache(txs, blockHeight, pool.chainConf.ChainConfig().Contract.EnableSqlSupport)
	pool.log.Infof("AddTxsToPendingCache, txs:%d, elapse time: [%dms]",
		len(txs), utils.CurrentTimeMillisSeconds()-startTime)
}

// RetryAndRemoveTxs Process transactions within multiple proposed blocks at the same height to
// ensure that these transactions are not lost, re-add valid txs which that are not on local node.
// remove txs in the commit block.
func (pool *txPoolImpl) RetryAndRemoveTxs(retryTxs []*commonPb.Transaction, removeTxs []*commonPb.Transaction) {
	if len(retryTxs) == 0 && len(removeTxs) == 0 {
		return
	}
	start := utils.CurrentTimeMillisSeconds()
	pool.retryTxs(retryTxs)
	pool.removeTxs(removeTxs)
	pool.log.Infof("RetryAndRemoveTxs, len(retryTxs):%d, len(removeTxs):%d, elapse time: [%dms]",
		len(retryTxs), len(removeTxs), utils.CurrentTimeMillisSeconds()-start)
}

// retryTxs re-add the txs to txPool
func (pool *txPoolImpl) retryTxs(txs []*commonPb.Transaction) {
	if len(txs) == 0 {
		return
	}
	var (
		configTxs   = make([]*commonPb.Transaction, 0)
		commonTxs   = make([]*commonPb.Transaction, 0)
		commonTxIds = make([]string, 0, len(txs))
		configTxIds = make([]string, 0, len(txs))
	)
	enableSqlDB := pool.chainConf.ChainConfig().Contract.EnableSqlSupport
	for _, tx := range txs {
		if utils.IsConfigTx(tx) ||
			utils.IsManageContractAsConfigTx(tx, enableSqlDB) ||
			utils.IsManagementTx(tx) {
			configTxs = append(configTxs, tx)
			configTxIds = append(configTxIds, tx.Payload.TxId)
		} else {
			commonTxs = append(commonTxs, tx)
			commonTxIds = append(commonTxIds, tx.Payload.TxId)
		}
	}

	pool.queue.deleteTxsInPending(txs)
	if len(configTxs) > 0 {
		mTxs := pool.validateTxs(configTxs, protocol.INTERNAL)
		pool.log.Debugf("retry config txs count: %d, after validate:%d, txIds: %d",
			len(configTxs), len(mTxs), len(configTxIds))
		if len(mTxs) > 0 {
			pool.queue.addTxsToConfigQueue(&mempoolTxs{isConfigTxs: true, mtxs: mTxs, source: protocol.INTERNAL})
		}
	}
	if len(commonTxs) > 0 {
		mTxs := pool.validateTxs(commonTxs, protocol.INTERNAL)
		pool.log.Debugf("retry common txs count: %d, after validate:%d, txIds: %d",
			len(commonTxs), len(mTxs), len(commonTxIds))
		if len(mTxs) > 0 {
			pool.queue.addTxsToCommonQueue(&mempoolTxs{isConfigTxs: false, mtxs: mTxs, source: protocol.INTERNAL})
		}
	}
}

// removeTxs delete the mtxs from the pool
func (pool *txPoolImpl) removeTxs(txs []*commonPb.Transaction) {
	if len(txs) == 0 {
		return
	}
	defer pool.updateAndPublishSignal()
	configTxIds := make([]string, 0, 1)
	commonTxIds := make([]string, 0, len(txs)/2)
	enableSqlDB := pool.chainConf.ChainConfig().Contract.EnableSqlSupport
	for _, tx := range txs {
		if utils.IsConfigTx(tx) ||
			utils.IsManageContractAsConfigTx(tx, enableSqlDB) ||
			utils.IsManagementTx(tx) {
			configTxIds = append(configTxIds, tx.Payload.TxId)
		} else {
			commonTxIds = append(commonTxIds, tx.Payload.TxId)
		}
	}

	if len(configTxIds) > 0 {
		pool.log.Debugf("removeTxBatch config txs count: %d, txIds: %d", len(configTxIds), len(configTxIds))
		pool.queue.deleteConfigTxs(configTxIds)
	}
	if len(commonTxIds) > 0 {
		pool.log.Debugf("removeTxBatch common txs count: %d, txIds: %d", len(commonTxIds), len(commonTxIds))
		pool.queue.deleteCommonTxs(commonTxIds)
	}
}

// TxExists verifies whether the transaction exists in the tx_pool
func (pool *txPoolImpl) TxExists(tx *commonPb.Transaction) bool {
	return pool.queue.has(tx, true)
}

func (pool *txPoolImpl) metrics(msg string, startTime int64, endTime int64) {
	if IsMetrics() {
		pool.log.Infof(msg, "internal", endTime-startTime, "startTime", startTime, "endTime", endTime)
	}
}

// OnMessage Process messages from MsgBus
func (pool *txPoolImpl) OnMessage(msg *msgbus.Message) {
	if msg == nil {
		pool.log.Errorf("receiveOnMessage msg OnMessage msg is empty")
		return
	}
	if msg.Topic != msgbus.RecvTxPoolMsg {
		pool.log.Errorf("receiveOnMessage msg topic is not msgbus.RecvTxPoolMsg")
		return
	}

	var (
		tx    = commonPb.Transaction{}
		bytes = msg.Payload.(*netPb.NetMsg).Payload
	)
	if err := proto.Unmarshal(bytes, &tx); err != nil {
		pool.log.Errorf("receiveOnMessage proto.Unmarshal(bytes, tx) err: %s", err)
		return
	}
	if err := pool.AddTx(&tx, protocol.P2P); err != nil {
		pool.log.Debugf("receiveOnMessage txId: %s, add failed: %s", tx.Payload.TxId, err.Error())
	}
	pool.log.Debugf("receiveOnMessage txId: %s, add success", tx.Payload.TxId)
}

func (pool *txPoolImpl) OnQuit() {
	// no implement
}

// convertMapToSlice convert txs map to slice
func convertMapToSlice(mtxs map[string]*memTx) []*memTx {
	mtxsRet := make([]*memTx, 0, len(mtxs))
	for _, mtx := range mtxs {
		mtxsRet = append(mtxsRet, mtx)
	}
	return mtxsRet
}

// setSignalStatus set txPool signal status
func (pool *txPoolImpl) setSignalStatus(signal txpoolPb.SignalType) {
	pool.signalLock.Lock()
	defer pool.signalLock.Unlock()
	pool.signalStatus = signal
}

//func (pool *txPoolImpl) getSignalStatus() txpoolPb.SignalType {
//	pool.signalLock.RLock()
//	defer pool.signalLock.RUnlock()
//	return pool.signalStatus
//}

func (pool *txPoolImpl) monitorValidateTxInDB(validateTimeMs int64) {
	if MonitorEnabled {
		go pool.metricTxExistInDBTime.WithLabelValues(pool.chainId).Observe(float64(validateTimeMs))
	}
}

func (pool *txPoolImpl) monitorValidateTxSign(validateTimeMs int64) {
	if MonitorEnabled {
		go pool.metricTxVerifySignTime.WithLabelValues(pool.chainId).Observe(float64(validateTimeMs))
	}
}
