VERSION221=v2.2.1
VERSION222=v2.2.2

gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/pb-go/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION222)
	go get chainmaker.org/chainmaker/utils/v2@$(VERSION222)
	go mod tidy
