/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package single

import (
	"chainmaker.org/chainmaker/common/v2/msgbus"
	msgbusmock "chainmaker.org/chainmaker/common/v2/msgbus/mock"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/mock"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"github.com/golang/mock/gomock"
)

var (
	MODULE_BRIEF = "[Brief]"
	MODULE_EVENT = "[Event]"
)

type mockTxFilter struct {
	txs      map[string]*commonPb.Transaction
	txFilter protocol.TxFilter
}

func newMockTxFilter(ctrl *gomock.Controller, txsMap map[string]*commonPb.Transaction) *mockTxFilter {
	filter := mock.NewMockTxFilter(ctrl)
	mockFilter := &mockTxFilter{txFilter: filter, txs: txsMap}

	filter.EXPECT().IsExistsAndReturnHeight(gomock.Any(), gomock.Any()).DoAndReturn(
		func(txId string, ruleType ...commonPb.RuleType) (bool, uint64, error) {
			_, exist := mockFilter.txs[txId]
			return exist, 0, nil
		}).AnyTimes()
	return mockFilter
}

type mockBlockChainStore struct {
	txs   map[string]*commonPb.Transaction
	store protocol.BlockchainStore
}

func newMockBlockChainStore(ctrl *gomock.Controller, txsMap map[string]*commonPb.Transaction) *mockBlockChainStore {
	store := mock.NewMockBlockchainStore(ctrl)
	mockStore := &mockBlockChainStore{store: store, txs: txsMap}

	store.EXPECT().GetTx(gomock.Any()).DoAndReturn(func(txId string) (*commonPb.Transaction, error) {
		tx := mockStore.txs[txId]
		return tx, nil
	}).AnyTimes()
	store.EXPECT().TxExists(gomock.Any()).DoAndReturn(func(txId string) (bool, error) {
		_, exist := mockStore.txs[txId]
		return exist, nil
	}).AnyTimes()
	store.EXPECT().TxExistsInFullDB(gomock.Any()).DoAndReturn(func(txId string) (bool, uint64, error) {
		_, exist := mockStore.txs[txId]
		return exist, 0, nil
	}).AnyTimes()

	store.EXPECT().TxExistsInIncrementDB(gomock.Any(), gomock.Any()).DoAndReturn(
		func(txId string, startHeight uint64) (bool, error) {
			_, exist := mockStore.txs[txId]
			return exist, nil
		}).AnyTimes()

	return mockStore
}

func newMockLogger(ctrl *gomock.Controller, name string) protocol.Logger {
	return &test.GoLogger{}
}

//
//func createLoggerByChain(name, chainId string) (*zap.SugaredLogger, log.LOG_LEVEL) {
//	var config log.LogConfig
//	var pureName string
//
//	logConfig := DefaultLogConfig()
//
//	if logConfig.SystemLog.LogLevelDefault == "" {
//		defaultLogNode := GetDefaultLogNodeConfig()
//		config = log.LogConfig{
//			Module:       "[DEFAULT]",
//			ChainId:      chainId,
//			LogPath:      defaultLogNode.FilePath,
//			LogLevel:     log.GetLogLevel(defaultLogNode.LogLevelDefault),
//			MaxAge:       defaultLogNode.MaxAge,
//			RotationTime: defaultLogNode.RotationTime,
//			JsonFormat:   false,
//			ShowLine:     true,
//			LogInConsole: defaultLogNode.LogInConsole,
//			ShowColor:    defaultLogNode.ShowColor,
//			IsBrief:      false,
//		}
//	} else {
//		if name == MODULE_BRIEF {
//			config = log.LogConfig{
//				Module:       name,
//				ChainId:      chainId,
//				LogPath:      logConfig.BriefLog.FilePath,
//				LogLevel:     log.GetLogLevel(logConfig.BriefLog.LogLevelDefault),
//				MaxAge:       logConfig.BriefLog.MaxAge,
//				RotationTime: logConfig.BriefLog.RotationTime,
//				RotationSize: logConfig.BriefLog.RotationSize,
//				JsonFormat:   false,
//				ShowLine:     true,
//				LogInConsole: logConfig.BriefLog.LogInConsole,
//				ShowColor:    logConfig.BriefLog.ShowColor,
//				IsBrief:      true,
//			}
//		} else if name == MODULE_EVENT {
//			config = log.LogConfig{
//				Module:       name,
//				ChainId:      chainId,
//				LogPath:      logConfig.EventLog.FilePath,
//				LogLevel:     log.GetLogLevel(logConfig.EventLog.LogLevelDefault),
//				MaxAge:       logConfig.EventLog.MaxAge,
//				RotationTime: logConfig.EventLog.RotationTime,
//				RotationSize: logConfig.EventLog.RotationSize,
//				JsonFormat:   false,
//				ShowLine:     true,
//				LogInConsole: logConfig.EventLog.LogInConsole,
//				ShowColor:    logConfig.EventLog.ShowColor,
//				IsBrief:      false,
//			}
//		} else {
//			pureName = strings.ToLower(strings.Trim(name, "[]"))
//			value, exists := logConfig.SystemLog.LogLevels[pureName]
//			if !exists {
//				value = logConfig.SystemLog.LogLevelDefault
//			}
//			config = log.LogConfig{
//				Module:       name,
//				ChainId:      chainId,
//				LogPath:      logConfig.SystemLog.FilePath,
//				LogLevel:     log.GetLogLevel(value),
//				MaxAge:       logConfig.SystemLog.MaxAge,
//				RotationTime: logConfig.SystemLog.RotationTime,
//				RotationSize: logConfig.SystemLog.RotationSize,
//				JsonFormat:   false,
//				ShowLine:     true,
//				LogInConsole: logConfig.SystemLog.LogInConsole,
//				ShowColor:    logConfig.SystemLog.ShowColor,
//				IsBrief:      false,
//			}
//		}
//	}
//
//	logger, _ := log.InitSugarLogger(&config)
//	return logger, config.LogLevel
//}

// DefaultLogConfig create default config for log module
//func DefaultLogConfig() *LogConfig {
//	defaultLogNode := GetDefaultLogNodeConfig()
//	config := &LogConfig{
//		SystemLog: LogNodeConfig{
//			LogLevelDefault: defaultLogNode.LogLevelDefault,
//			FilePath:        defaultLogNode.FilePath,
//			MaxAge:          defaultLogNode.MaxAge,
//			RotationTime:    defaultLogNode.RotationTime,
//			RotationSize:    defaultLogNode.RotationSize,
//			LogInConsole:    defaultLogNode.LogInConsole,
//		},
//		BriefLog: LogNodeConfig{
//			LogLevelDefault: defaultLogNode.LogLevelDefault,
//			FilePath:        defaultLogNode.FilePath,
//			MaxAge:          defaultLogNode.MaxAge,
//			RotationTime:    defaultLogNode.RotationTime,
//			RotationSize:    defaultLogNode.RotationSize,
//			LogInConsole:    defaultLogNode.LogInConsole,
//		},
//		EventLog: LogNodeConfig{
//			LogLevelDefault: defaultLogNode.LogLevelDefault,
//			FilePath:        defaultLogNode.FilePath,
//			MaxAge:          defaultLogNode.MaxAge,
//			RotationTime:    defaultLogNode.RotationTime,
//			RotationSize:    defaultLogNode.RotationSize,
//			LogInConsole:    defaultLogNode.LogInConsole,
//		},
//	}
//	return config
//}

// GetDefaultLogNodeConfig create a default log config of node
//func GetDefaultLogNodeConfig() LogNodeConfig {
//	return LogNodeConfig{
//		LogLevelDefault: log.INFO,
//		FilePath:        "./default.log",
//		MaxAge:          log.DEFAULT_MAX_AGE,
//		RotationTime:    log.DEFAULT_ROTATION_TIME,
//		RotationSize:    log.DEFAULT_ROTATION_SIZE,
//		LogInConsole:    true,
//		ShowColor:       true,
//	}
//}

// LogConfig the config of log module
type LogConfig struct {
	ConfigFile string        `mapstructure:"config_file"`
	SystemLog  LogNodeConfig `mapstructure:"system"`
	BriefLog   LogNodeConfig `mapstructure:"brief"`
	EventLog   LogNodeConfig `mapstructure:"event"`
}

// LogNodeConfig the log config of node
type LogNodeConfig struct {
	LogLevelDefault string            `mapstructure:"log_level_default"`
	LogLevels       map[string]string `mapstructure:"log_levels"`
	FilePath        string            `mapstructure:"file_path"`
	MaxAge          int               `mapstructure:"max_age"`
	RotationTime    int               `mapstructure:"rotation_time"`
	RotationSize    int64             `mapstructure:"rotation_size"`
	LogInConsole    bool              `mapstructure:"log_in_console"`
	ShowColor       bool              `mapstructure:"show_color"`
}

func newMockChainConf(ctrl *gomock.Controller) protocol.ChainConf {
	mockCC := mock.NewMockChainConf(ctrl)
	mockCC.EXPECT().ChainConfig().AnyTimes().DoAndReturn(func() *configPb.ChainConfig {
		return &configPb.ChainConfig{
			Block: &configPb.BlockConfig{
				TxTimestampVerify: false,
				TxTimeout:         0,
				BlockTxCapacity:   DefaultMaxTxCount,
			},
			Contract: &configPb.ContractConfig{},
		}
	})

	return mockCC
}

func newMockMessageBus(ctrl *gomock.Controller) msgbus.MessageBus {
	mockMsgBus := msgbusmock.NewMockMessageBus(ctrl)
	mockMsgBus.EXPECT().Register(gomock.Any(), gomock.Any()).AnyTimes()
	mockMsgBus.EXPECT().Publish(gomock.Any(), gomock.Any()).AnyTimes()
	return mockMsgBus
}

func newMockAccessControlProvider(ctrl *gomock.Controller) protocol.AccessControlProvider {
	mockAc := mock.NewMockAccessControlProvider(ctrl)
	return mockAc
}

//func newMockNet(ctrl *gomock.Controller) protocol.NetService {
//	mockNet := mock.NewMockNetService(ctrl)
//	return mockNet
//}
